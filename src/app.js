const express = require('express');
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
const app = express();
const port = 8000;
const db = require('../config/db');
const routes = require('./routes')

app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

mongoose.connect(db.url, { useNewUrlParser: true })

app.use(express.json());

routes(app)

app.listen(port, () => {
	console.log('We are live on ' + port);
});
