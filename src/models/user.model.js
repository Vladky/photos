const config = require('config')
const jwt = require('jsonwebtoken')
const Joi = require('joi')
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
	login: {
		type: String,
		required: true,
		minlength: 3,
		maxlength: 50
	},
	password: {
		type: String,
		required: true,
		minlength: 3,
		maxlength: 255
	},
	isAdmin: {
		type: Boolean
	}
})

UserSchema.methods.generateAuthToken = function() {
	const token = jwt.sign(
		{ _id: this._id, isAdmin: this.isAdmin },
		config.get('myprivatekey')
	) //get the private key from the config file -> environment variable
	return token
}

const User = mongoose.model('User', UserSchema)

function validateUser(user) {
	const schema = {
		login: Joi.string()
			.min(3)
			.max(50)
			.required(),
		password: Joi.string()
			.min(3)
			.max(255)
			.required(),
		isAdmin: Joi.boolean()
	}

	return Joi.validate(user, schema)
}

exports.User = User
exports.validate = validateUser
