const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const photoScheme = new Schema({
	url: String,
});

const Photo = mongoose.model("photos", photoScheme);

module.exports = Photo
