const Model = require('../models/photo.model')

module.exports = {
	get: (params = {}) => new Promise((resolve, reject) => {
		Model.find(params, (err, docs) => {
			if (!!err) {
				reject(err)
			}
			resolve(docs)
		})
	}),
	add: model => new Promise((resolve, reject) => {
		new Model(model).save().then(() => {
			resolve()
		}).catch(() => {
			reject()
		})
	})
}
