const service = require('../service/photoService')
const auth = require("../../middleware/auth");
const express = require("express");
const router = express.Router();

router.get('/', (req, res) => {
	service.get(req.body).then(result => {
		res.send(result)
	})
})
router.post('/', auth, (req, res) => {
	service.add(req.body).then(() => {
		res.send({ status: 200 })
	})
})

module.exports = router
