module.exports = app => {
	app.use("/api/photos", require('./photo.route'));
	app.use("/api/users", require('./user.route'));
};
