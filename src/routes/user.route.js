const auth = require("../../middleware/auth");
const bcrypt = require("bcrypt");
const { User, validate } = require("../models/user.model");
const express = require("express");
const router = express.Router();

router.get("/current", auth, async (req, res) => {
	const user = await User.findById(req.user._id).select("-password");
	res.send(user);
});

router.post("/signin", async (req, res) => {
	const { error } = validate(req.body);
	if (error) return res.status(400).send(error.details[0].message);

	let user = await User.findOne({ login: req.body.login });
	if (user) return res.status(400).send("User already registered.");

	user = new User({
		login: req.body.login,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	});
	user.password = await bcrypt.hash(user.password, 10);
	await user.save();

	const token = user.generateAuthToken();
	res.header("x-auth-token", token).send({
		_id: user._id,
		login: user.login,
	});
});

router.post('/auth', async (req, res) => {

	console.log("TCL: req.body", req.body)
	let user = await User.findOne({ login: req.body.login });
  console.log("TCL: user", user)
	if (!!user && !!req.body.password && await bcrypt.compare(req.body.password, user.password)) {
		const token = user.generateAuthToken();
		res.header("x-auth-token", token).send({
			_id: user._id,
			login: user.login,
		});
	} else {
		return res.status(403).send("Invalid authorization data");
	}
})

module.exports = router;