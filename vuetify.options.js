export default {
	breakpoint: {
		thresholds: {
			xs: 340,
			sm: 540,
			md: 800,
			lg: 1280,
		},
		scrollBarWidth: 24,
	},
	theme: {
		dark: false
	}
}