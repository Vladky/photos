import axios from 'axios'
const endpoint = 'http://localhost:8000/api/users/auth'

export default {
	login: data => axios.post(endpoint, data)
}
