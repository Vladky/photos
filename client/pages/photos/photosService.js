import axios from 'axios'
const endpoint = 'http://localhost:8000/api/photos'

export default {
	get: params => axios.get(endpoint, { params: params })
}
