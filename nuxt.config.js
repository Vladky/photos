module.exports = {
	srcDir: 'client/',
	head: {
		title: 'Фотографии',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Nuxt.js project' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
		]
	},
	loading: { color: '#3B8070' },
	plugins: [
		// { src: '~/plugins/vuetify.js' }
	],
	css: [
		{ src: '~/assets/fonts/style.css' },
		{ src: '~/assets/css/index.scss', lang: 'scss' }
	],
	build: {
		srcDir: 'client/',
		extend(config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				})
			}
		},
		extractCSS: true,
	},
	buildModules: [
		'@nuxtjs/vuetify'
	],
	vuetify: {
		optionsPath: './vuetify.options.js'
	},
	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/auth'
	],
	serverMiddleware: ['./middleware/auth.js'],
	// auth: {
	// 	strategies: {
	// 		local: {
	// 			endpoints: {
	// 				login: { url: '/api/users/signin', method: 'post', propertyName: 'x-csrf-token' },
	// 			},
	// 		},
	// 	}
	// }
}

